package roleOfLocking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author elbatista
 */
public class Results {
    
    public void generateResults(String dir, int numThreads, boolean generateResults, boolean generateGraphs) throws IOException {
        if(!generateResults) return;
        PrintWriter pw = new PrintWriter(new FileWriter(new File(dir+"/plot_data_"+numThreads+"t.p")));
        pw.println(
            "set key outside font \"Helvetica, 14\"\n" +
            "set xlabel \"Execution time (seconds)\"\n" +
            "set ylabel \"Throughput (kops)\"\n" +
            "set grid ytics lt 0 lw 1\n" +
            "set terminal pdf dashed size 8, 3\n" +
            "set xlabel font \"Helvetica,18\"\n" +
            "set ylabel font \"Helvetica,18\"\n" +
            "set xtics font \"Helvetica, 14\"\n" +
            "set ytics font \"Helvetica, 14\"\n" +
            "set x2tics font \"Helvetica, 14\"\n" +
            "set yrange [0:20]\n" +
            "set output '"+dir+"/tp_run_"+numThreads+"t.pdf'\n" +
            "plot \\\n" +
            "\""+dir+"/throughput_sequential.txt\" using 1:($2/1000) title \"Sequential\"   with line lc \"black\" dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_coarse_"+numThreads+"t.txt\" using 1:($2/1000) title \"Coarse\"   with line lc \"green\" dashtype 1 lw 2, \\\n" +
            "\""+dir+"/throughput_fine_"+numThreads+"t.txt\" using 1:($2/1000) title \"Fine\"       with line lc \"blue\" dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_opt_"+numThreads+"t.txt\" using 1:($2/1000) title \"Optimistic\"  with line lc \"orange\" dashtype 4 lw 2, \\\n" +
            "\""+dir+"/throughput_lazy_"+numThreads+"t.txt\" using 1:($2/1000) title \"Lazy\"       with line lc \"red\" dashtype 3 lw 3, \\\n" +
            "\""+dir+"/throughput_lockFree_"+numThreads+"t.txt\" using 1:($2/1000) title \"LockFree\"  with line lc \"purple\" dashtype 5 lw 2 \n" +
            "\n" +
            "set output '"+dir+"/tp_run_ops_"+numThreads+"t.pdf'\n" +
            "plot \\\n" +
            "\""+dir+"/throughput_coarse_"+numThreads+"t.txt\" using 1:($3/1000) title \"Coarse Add\" with line lc 2 dashtype 1 lw 2, \\\n" +
            "\""+dir+"/throughput_coarse_"+numThreads+"t.txt\" using 1:($4/1000) title \"Coarse Remove\" with line lc 3 dashtype 1 lw 2, \\\n" +
            "\""+dir+"/throughput_coarse_"+numThreads+"t.txt\" using 1:($5/1000) title \"Coarse Contains\" with line lc 4 dashtype 1 lw 2, \\\n" +
            "\""+dir+"/throughput_fine_"+numThreads+"t.txt\" using 1:($3/1000) title \"Fine Add\" with line lc 8 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_fine_"+numThreads+"t.txt\" using 1:($4/1000) title \"Fine Remove\" with line lc 9 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_fine_"+numThreads+"t.txt\" using 1:($5/1000) title \"Fine Contains\" with line lc 10 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_opt_"+numThreads+"t.txt\" using 1:($3/1000) title \"Opt Add\" with line lc 11 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_opt_"+numThreads+"t.txt\" using 1:($4/1000) title \"Opt Remove\" with line lc 12 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_opt_"+numThreads+"t.txt\" using 1:($5/1000) title \"Opt Contains\" with line lc 13 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lazy_"+numThreads+"t.txt\" using 1:($3/1000) title \"Lazy Add\" with line lc 8 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lazy_"+numThreads+"t.txt\" using 1:($4/1000) title \"Lazy Remove\" with line lc 9 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lazy_"+numThreads+"t.txt\" using 1:($5/1000) title \"Lazy Contains\" with line lc 10 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lockFree_"+numThreads+"t.txt\" using 1:($3/1000) title \"LockFree Add\" with line lc 8 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lockFree_"+numThreads+"t.txt\" using 1:($4/1000) title \"LockFree Remove\" with line lc 9 dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_lockFree_"+numThreads+"t.txt\" using 1:($5/1000) title \"LockFree Contains\" with line lc 10 dashtype 2 lw 2 \n" +
            "\n" +
            "set ylabel \"List size (kitems)\"\n" +
            "set output '"+dir+"/tp_run_pop_"+numThreads+"t.pdf'\n" +
            "set autoscale y \n" +
            "plot \\\n" +
            "\""+dir+"/throughput_sequential.txt\" using 1:($6/1000) title \"Seq Population\"    with line lc \"black\" dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_coarse_"+numThreads+"t.txt\" using 1:($6/1000) title \"Coarse Population\"    with line lc \"green\" dashtype 1 lw 2, \\\n" +
            "\""+dir+"/throughput_fine_"+numThreads+"t.txt\" using 1:($6/1000) title \"Fine Population\"        with line lc \"blue\" dashtype 2 lw 2, \\\n" +
            "\""+dir+"/throughput_opt_"+numThreads+"t.txt\" using 1:($6/1000) title \"Opt Population\"          with line lc \"orange\" dashtype 4 lw 2, \\\n" +
            "\""+dir+"/throughput_lazy_"+numThreads+"t.txt\" using 1:($6/1000) title \"Lazy Population\"        with line lc \"red\" dashtype 3 lw 3, \\\n" +
            "\""+dir+"/throughput_lockFree_"+numThreads+"t.txt\" using 1:($6/1000) title \"LockFree Population\" with line lc \"purple\" dashtype 3 lw 3 \n" 
        );
        pw.flush();
        pw.close();
        if(generateGraphs) gnuplot(dir+"/plot_data_"+numThreads+"t.p");
    }
    
    public void consolidateFiles(String dir, boolean generateGraphs) throws IOException {
        
        PrintWriter pw5 = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_seq.txt")));
        File file = new File(dir+"/throughput_sequential.txt");
        double [] seqData;
        seqData = calcAvgsFromFile(file.toPath());
        pw5.println(1+"\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        // só pra aparecer no grafico:
        pw5.println("1.5"+"\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        pw5.flush();
        
        PrintWriter pw = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_coarse.txt")));
        List<File>list = Arrays.asList(new File(dir).listFiles((a,name)->name.startsWith("throughput_coarse_")));
        list.sort((a, b) -> {
            Integer aa = Integer.valueOf(new StringBuilder(new StringBuilder(a.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            Integer bb = Integer.valueOf(new StringBuilder(new StringBuilder(b.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            return aa.compareTo(bb);
        });
        pw.println("Seq\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        list.stream().forEach(item->{
            String []arrayName = item.getName().split("_");
            int numThreads = Integer.valueOf(new StringBuilder(new StringBuilder(arrayName[2]).reverse().substring(5)).reverse().toString());
            double [] data = calcAvgsFromFile(item.toPath());
            pw.println(numThreads+"\t"+data[0]+"\t"+data[1]+"\t"+data[2]+"\t"+data[3]+"\t"+data[4]+"\t"+data[5]+"\t"+data[6]+"\t"+data[7]+"\t"+data[8]+"\t"+data[9]);
        });
        pw.flush();
        
        PrintWriter pw2 = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_fine.txt")));
        list = Arrays.asList(new File(dir).listFiles((a,name)->name.startsWith("throughput_fine_")));
        list.sort((a, b) -> {
            Integer aa = Integer.valueOf(new StringBuilder(new StringBuilder(a.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            Integer bb = Integer.valueOf(new StringBuilder(new StringBuilder(b.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            return aa.compareTo(bb);
        });
        pw2.println("Seq\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        list.stream().forEach(item->{
            String []arrayName = item.getName().split("_");
            int numThreads = Integer.valueOf(new StringBuilder(new StringBuilder(arrayName[2]).reverse().substring(5)).reverse().toString());
            double [] data = calcAvgsFromFile(item.toPath());
            pw2.println(numThreads+"\t"+data[0]+"\t"+data[1]+"\t"+data[2]+"\t"+data[3]+"\t"+data[4]+"\t"+data[5]+"\t"+data[6]+"\t"+data[7]+"\t"+data[8]+"\t"+data[9]);
        });
        pw2.flush();
        
        PrintWriter pw3 = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_opt.txt")));
        list = Arrays.asList(new File(dir).listFiles((a,name)->name.startsWith("throughput_opt_")));
        list.sort((a, b) -> {
            Integer aa = Integer.valueOf(new StringBuilder(new StringBuilder(a.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            Integer bb = Integer.valueOf(new StringBuilder(new StringBuilder(b.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            return aa.compareTo(bb);
        });
        pw3.println("Seq\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        list.stream().forEach(item->{
            String []arrayName = item.getName().split("_");
            int numThreads = Integer.valueOf(new StringBuilder(new StringBuilder(arrayName[2]).reverse().substring(5)).reverse().toString());
            double [] data = calcAvgsFromFile(item.toPath());
            pw3.println(numThreads+"\t"+data[0]+"\t"+data[1]+"\t"+data[2]+"\t"+data[3]+"\t"+data[4]+"\t"+data[5]+"\t"+data[6]+"\t"+data[7]+"\t"+data[8]+"\t"+data[9]);
        });
        pw3.flush();
        
        PrintWriter pw4 = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_lazy.txt")));
        list = Arrays.asList(new File(dir).listFiles((a,name)->name.startsWith("throughput_lazy_")));
        list.sort((a, b) -> {
            Integer aa = Integer.valueOf(new StringBuilder(new StringBuilder(a.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            Integer bb = Integer.valueOf(new StringBuilder(new StringBuilder(b.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            return aa.compareTo(bb);
        });
        pw4.println("Seq\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        list.stream().forEach(item->{
            String []arrayName = item.getName().split("_");
            int numThreads = Integer.valueOf(new StringBuilder(new StringBuilder(arrayName[2]).reverse().substring(5)).reverse().toString());
            double [] data = calcAvgsFromFile(item.toPath());
            pw4.println(numThreads+"\t"+data[0]+"\t"+data[1]+"\t"+data[2]+"\t"+data[3]+"\t"+data[4]+"\t"+data[5]+"\t"+data[6]+"\t"+data[7]+"\t"+data[8]+"\t"+data[9]);
        });
        pw4.flush();
       
        PrintWriter pw6 = new PrintWriter(new FileWriter(new File(dir+"/avg_throughput_lockFree.txt")));
        list = Arrays.asList(new File(dir).listFiles((a,name)->name.startsWith("throughput_lockFree_")));
        list.sort((a, b) -> {
            Integer aa = Integer.valueOf(new StringBuilder(new StringBuilder(a.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            Integer bb = Integer.valueOf(new StringBuilder(new StringBuilder(b.getName().split("_")[2]).reverse().substring(5)).reverse().toString());
            return aa.compareTo(bb);
        });
        pw6.println("Seq\t"+seqData[0]+"\t"+seqData[1]+"\t"+seqData[2]+"\t"+seqData[3]+"\t"+seqData[4]+"\t"+seqData[5]+"\t"+seqData[6]+"\t"+seqData[7]+"\t"+seqData[8]+"\t"+seqData[9]);
        list.stream().forEach(item->{
            String []arrayName = item.getName().split("_");
            int numThreads = Integer.valueOf(new StringBuilder(new StringBuilder(arrayName[2]).reverse().substring(5)).reverse().toString());
            double [] data = calcAvgsFromFile(item.toPath());
            pw6.println(numThreads+"\t"+data[0]+"\t"+data[1]+"\t"+data[2]+"\t"+data[3]+"\t"+data[4]+"\t"+data[5]+"\t"+data[6]+"\t"+data[7]+"\t"+data[8]+"\t"+data[9]);
        });
        pw6.flush();
       
        plotConsolidateFiles(dir, generateGraphs);
    }
    
    private void gnuplot(String plotFile) throws IOException{
        try {
            final Process p = Runtime.getRuntime().exec("gnuplot "+plotFile);
            Thread t = new Thread(new Runnable() {
                public void run() {
                 BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                 String line = null; 
                 try {
                    while ((line = input.readLine()) != null)
                        System.out.println(line);
                 } catch (IOException e) {
                        e.printStackTrace();
                 }
                }
            });
            t.start();
            t.join();
            p.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private double[] calcAvgsFromFile(Path path) {
        double [] ret = new double[10];
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line = br.readLine();
            Double sumAllOps=0.0, avgAllOps=0.0, standardDeviationAllOps=0.0;
            Double sumAdd=0.0, avgAdd=0.0, standardDeviationAdd=0.0;
            Double sumRemove=0.0, avgRemove=0.0, standardDeviationRemove=0.0;
            Double sumContains=0.0, avgContains=0.0, standardDeviationContains=0.0;
            Double sumPop=0.0, avgPop=0.0, standardDeviationPop=0.0;
            int cont = 0;
            ArrayList<Double> arrayAllOps = new ArrayList<>();
            ArrayList<Double> arrayAdd = new ArrayList<>();
            ArrayList<Double> arrayRemove = new ArrayList<>();
            ArrayList<Double> arrayContains = new ArrayList<>();
            ArrayList<Double> arrayPop = new ArrayList<>();
            while (line != null) {
                String [] str = line.split("\t");
                sumAllOps += Double.parseDouble(str[1].replace(",", "."));
                sumAdd += Double.parseDouble(str[2].replace(",", "."));
                sumRemove += Double.parseDouble(str[3].replace(",", "."));
                sumContains += Double.parseDouble(str[4].replace(",", "."));
                sumPop += Double.parseDouble(str[5].replace(",", "."));

                line = br.readLine();
                cont++;
                
                arrayAllOps.add(Double.parseDouble(str[1].replace(",", ".")));
                arrayAdd.add(Double.parseDouble(str[2].replace(",", ".")));
                arrayRemove.add(Double.parseDouble(str[3].replace(",", ".")));
                arrayContains.add(Double.parseDouble(str[4].replace(",", ".")));
                arrayPop.add(Double.parseDouble(str[5].replace(",", ".")));
            }
            br.close();
            
            avgAllOps = sumAllOps/cont;
            ret[0] = (avgAllOps);
            for(Double i : arrayAllOps) standardDeviationAllOps += Math.pow(i - avgAllOps, 2);
            ret[1]=(Math.sqrt(standardDeviationAllOps/cont));
            
            avgAdd = sumAdd/cont;
            ret[2] = (avgAdd);
            for(Double i : arrayAdd) standardDeviationAdd += Math.pow(i - avgAdd, 2);
            ret[3]=(Math.sqrt(standardDeviationAdd/cont));
            
            avgRemove = sumRemove/cont;
            ret[4] = (avgRemove);
            for(Double i : arrayRemove) standardDeviationRemove += Math.pow(i - avgRemove, 2);
            ret[5]=(Math.sqrt(standardDeviationRemove/cont));
            
            avgContains = sumContains/cont;
            ret[6] = (avgContains);
            for(Double i : arrayContains) standardDeviationContains += Math.pow(i - avgContains, 2);
            ret[7]=(Math.sqrt(standardDeviationContains/cont));
            
            avgPop = sumPop/cont;
            ret[8] = (avgPop);
            for(Double i : arrayPop) standardDeviationPop += Math.pow(i - avgPop, 2);
            ret[9]=(Math.sqrt(standardDeviationPop/cont));
            
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        return ret;
    }

    private void plotConsolidateFiles(String dir, boolean generateGraphs) throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter(new File(dir+"/plot_avg_data.p")));
        pw.print(
            "set key left top font \"Helvetica, 10\"\n" +
            "set output '"+dir+"/tp_cons_coarse.pdf'\n" +
            "set style fill pattern border lt -1\n" +
            "set terminal pdf dashed size 4.5, 2.5\n" +
            "set style data histogram\n" +
            "set style histogram errorbars gap 2 lw 2\n" +
            "set xlabel font \"Helvetica,15\"\n" +
            "set ylabel font \"Helvetica,14\"\n" +
            "set xtics font \"Helvetica, 14\"\n" +
            "set ytics font \"Helvetica, 14\"\n" +
            "set grid ytics lt 0 lw 1\n" +
            "set xlabel \"Working Threads\"\n" +
            "set ylabel \"Avg throughput (kops)\"\n" +
            "set yrange [0:20]\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_coarse.txt' using ($2/1000):($3/1000):xtic(1) ti \"General\", \\\n" +
            "'"+dir+"/avg_throughput_coarse.txt' using ($4/1000):($5/1000):xtic(1) ti \"Add\", \\\n" +
            "'"+dir+"/avg_throughput_coarse.txt' using ($6/1000):($7/1000):xtic(1) ti \"Remove\", \\\n" +
            "'"+dir+"/avg_throughput_coarse.txt' using ($8/1000):($9/1000):xtic(1) ti \"Contains\"\n" +
            "set output '"+dir+"/tp_cons_fine.pdf'\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_fine.txt' using ($2/1000):($3/1000):xtic(1) ti \"General\", \\\n" +
            "'"+dir+"/avg_throughput_fine.txt' using ($4/1000):($5/1000):xtic(1) ti \"Add\", \\\n" +
            "'"+dir+"/avg_throughput_fine.txt' using ($6/1000):($7/1000):xtic(1) ti \"Remove\", \\\n" +
            "'"+dir+"/avg_throughput_fine.txt' using ($8/1000):($9/1000):xtic(1) ti \"Contains\"\n" +
            "set output '"+dir+"/tp_cons_lazy.pdf'\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_lazy.txt' using ($2/1000):($3/1000):xtic(1) ti \"General\", \\\n" +
            "'"+dir+"/avg_throughput_lazy.txt' using ($4/1000):($5/1000):xtic(1) ti \"Add\", \\\n" +
            "'"+dir+"/avg_throughput_lazy.txt' using ($6/1000):($7/1000):xtic(1) ti \"Remove\", \\\n" +
            "'"+dir+"/avg_throughput_lazy.txt' using ($8/1000):($9/1000):xtic(1) ti \"Contains\"\n" +
            "set output '"+dir+"/tp_cons_optimistic.pdf'\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_opt.txt' using ($2/1000):($3/1000):xtic(1) ti \"General\", \\\n" +
            "'"+dir+"/avg_throughput_opt.txt' using ($4/1000):($5/1000):xtic(1) ti \"Add\", \\\n" +
            "'"+dir+"/avg_throughput_opt.txt' using ($6/1000):($7/1000):xtic(1) ti \"Remove\", \\\n" +
            "'"+dir+"/avg_throughput_opt.txt' using ($8/1000):($9/1000):xtic(1) ti \"Contains\"\n"+
            "set output '"+dir+"/tp_cons_lockFree.pdf'\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_lockFree.txt' using ($2/1000):($3/1000):xtic(1) ti \"General\", \\\n" +
            "'"+dir+"/avg_throughput_lockFree.txt' using ($4/1000):($5/1000):xtic(1) ti \"Add\", \\\n" +
            "'"+dir+"/avg_throughput_lockFree.txt' using ($6/1000):($7/1000):xtic(1) ti \"Remove\", \\\n" +
            "'"+dir+"/avg_throughput_lockFree.txt' using ($8/1000):($9/1000):xtic(1) ti \"Contains\"\n" +
            "set xlabel \"Num. Threads\"\n" +
            "set ylabel \"Throughput (kops/sec)\"\n"+
            "set xrange [0:128]\n" +
            "set xtics (1,2,4,8,16,32,64,128)\n" +
            "set yrange [0:6]\n" +
            "set output '"+dir+"/tp_all.pdf'\n" +
            "set terminal pdf dashed size 12, 5\n" +
            "plot \\\n" +
            "'"+dir+"/avg_throughput_seq.txt' using 1:($2/1000) title \"Sequential\" with line lc \"black\" dashtype 1 lw 5, \\\n" +
            "'"+dir+"/avg_throughput_coarse.txt' using 1:($2/1000) title \"Coarse\" with line lc \"purple\" dashtype 2 lw 2, \\\n" +
            "'"+dir+"/avg_throughput_fine.txt' using 1:($2/1000) title \"Fine\" with line lc \"green\" dashtype 1 lw 2, \\\n" +
            "'"+dir+"/avg_throughput_opt.txt' using 1:($2/1000) title \"Optimistic\" with line lc \"blue\" dashtype 2 lw 2, \\\n" +
            "'"+dir+"/avg_throughput_lazy.txt' using 1:($2/1000) title \"Lazy\" with line lc \"orange\" dashtype 4 lw 2, \\\n" +
            "'"+dir+"/avg_throughput_lockFree.txt' using 1:($2/1000) title \"Lock Free\" with line lc \"red\" dashtype 3 lw 3"
        );
        pw.flush();
        pw.close();
        if(generateGraphs) gnuplot(dir+"/plot_avg_data.p");
    }
    
}
