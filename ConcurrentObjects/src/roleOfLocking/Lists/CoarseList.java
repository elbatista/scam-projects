package roleOfLocking.Lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author elia.batista
 * @param <T>
 */
public class CoarseList <T> implements Set<T>{
    private class Node <T> {
        T item;
        int key;
        Node next;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
        }
    }
    private final Node head;
    private final AtomicInteger size = new AtomicInteger(0);
    private final Lock lock = new ReentrantLock();
    public CoarseList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new Node(Integer.MAX_VALUE);
    }
    
    @Override
    public int size(){
        return size.get();
    }
    
    @Override
    public boolean add(T item) {
        Node pred, curr;
        int key = item.hashCode();
        lock.lock();
        try {
            pred = head;
            curr = pred.next;
            while (curr.key < key) {
                pred = curr;
                curr = curr.next;
            }
            if (key == curr.key) {
                return false;
            } else {
                Node node = new Node(item);
                node.next = curr;
                pred.next = node;
                size.incrementAndGet();
                return true;
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean remove(T item) {
        Node pred, curr;
        int key = item.hashCode();
        lock.lock();
        try {
            pred = head;
            curr = pred.next;
            while (curr.key < key) {
                pred = curr;
                curr = curr.next;
            }
            if (key == curr.key) {
                pred.next = curr.next;
                size.decrementAndGet();
                return true;
            }
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean contains(T item) {
        Node pred, curr;
        int key = item.hashCode();
        lock.lock();
        try {
            pred = head;
            curr = pred.next;
            while (curr.key < key) {
                pred = curr;
                curr = curr.next;
            }
            if (key == curr.key) {
                pred.next = curr.next;
                return true;
            }
            return false;
        } finally {
            lock.unlock();
        }
    }
    
    @Override
    public String getFileName(int numThreads) {
        return "throughput_coarse_"+numThreads+"t.txt";
    }

    @Override
    public void init(int iniSize){
        System.out.println("Initializing coarse list with "+iniSize+" entries");
        Node pred, curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = node;
            pred = node;
            size.incrementAndGet();
        }
    }
}
