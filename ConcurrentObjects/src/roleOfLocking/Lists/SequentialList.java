/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roleOfLocking.Lists;

/**
 *
 * @author elbatista
 */
public class SequentialList<T> implements Set<T>{

    private class Node <T> {
        T item;
        int key;
        Node next;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
        }
    }
    private final Node head;
    private int size = 0;
    
    public SequentialList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new Node(Integer.MAX_VALUE);
    }
    
    @Override
    public int size(){
        return size;
    }
    
    @Override
    public boolean add(T item) {
        Node pred, curr;
        int key = item.hashCode();
        pred = head;
        curr = pred.next;
        while (curr.key < key) {
            pred = curr;
            curr = curr.next;
        }
        if (key == curr.key) {
            return false;
        } else {
            Node node = new Node(item);
            node.next = curr;
            pred.next = node;
            size++;
            return true;
        }
    }

    @Override
    public boolean remove(T item) {
        Node pred, curr;
        int key = item.hashCode();
        pred = head;
        curr = pred.next;
        while (curr.key < key) {
            pred = curr;
            curr = curr.next;
        }
        if (key == curr.key) {
            pred.next = curr.next;
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(T item) {
        Node pred, curr;
        int key = item.hashCode();
        pred = head;
        curr = pred.next;
        while (curr.key < key) {
            pred = curr;
            curr = curr.next;
        }
        if (key == curr.key) {
            pred.next = curr.next;
            return true;
        }
        return false;
    }

    @Override
    public String getFileName(int numThreads) {
        return "throughput_sequential.txt";
    }
    
    @Override
    public void init(int iniSize){
        System.out.println("Initializing sequential list with "+iniSize+" entries");
        Node pred, curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = node;
            pred = node;
            size++;
        }
    }
    
}
