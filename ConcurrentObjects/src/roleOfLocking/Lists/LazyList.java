/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roleOfLocking.Lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author elbatista
 */
public class LazyList<T> implements Set<T>{

    private class Node <T> {
        T item;
        int key;
        Node next;
        Lock lock;
        boolean marked;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
            lock = new ReentrantLock();
        }
        void lock(){lock.lock();}
        void unlock(){lock.unlock();}
    }
    private final Node head;
    private final AtomicInteger size = new AtomicInteger(0);
    
    public LazyList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new Node(Integer.MAX_VALUE);
    }
    
    private boolean validate(Node pred, Node curr) {
        return !pred.marked && !curr.marked && pred.next == curr;
    }

    @Override
    public int size(){
        return size.get();
    }

    @Override
    public boolean add(T item) {
        int key = item.hashCode();
        while (true) {
            Node pred = head;
            Node curr = head.next;
            while (curr.key < key) {
                pred = curr; curr = curr.next;
            }
            pred.lock();
            try {
                curr.lock();
                try {
                    if (validate(pred, curr)) {
                        if (curr.key == key) {
                            return false;
                        } else {
                            Node node = new Node(item);
                            node.next = curr;
                            pred.next = node;
                            size.incrementAndGet();
                            return true;
                        }
                    }
                } finally {
                    curr.unlock();
                }
            } finally {
                pred.unlock();
            }
        }
    }

    @Override
    public boolean remove(T item) {
        int key = item.hashCode();
        while (true) {
            Node pred = head;
            Node curr = head.next;
            while (curr.key < key) {
                pred = curr; curr = curr.next;
            }
            pred.lock();
            try {
                curr.lock();
                try {
                    if (validate(pred, curr)) {
                        if (curr.key != key) {
                            return false;
                        } else {
                            curr.marked = true;
                            pred.next = curr.next;
                            size.decrementAndGet();
                            return true;
                        }
                    }
                } finally {
                    curr.unlock();
                }
            } finally {
                pred.unlock();
            }
        }
    }

    @Override
    public boolean contains(T item) {
        int key = item.hashCode();
        Node curr = head;
        while (curr.key < key)
            curr = curr.next;
        return curr.key == key && !curr.marked;
    }
    
    @Override
    public String getFileName(int numThreads) {
        return "throughput_lazy_"+numThreads+"t.txt";
    }
    
    @Override
    public void init(int iniSize){
        System.out.println("Initializing lazy list with "+iniSize+" entries");
        Node pred, curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = node;
            pred = node;
            size.incrementAndGet();
        }
    }
}
