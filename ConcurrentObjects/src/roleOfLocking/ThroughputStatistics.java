package roleOfLocking;

import roleOfLocking.Lists.Set;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author elia.batista
 */
public class ThroughputStatistics {
    private int[][] counters;
    private int[][][] countersPerOp;
    private int period = 1000; //millis
    private int interval = 120, warmup = 5;
    private boolean started = false;
    private int now = 0;
    private int [] population;
    private PrintWriter pw;
    private int numT = 0;
    public static final int ADD=0,REMOVE=1,CONTAINS=2;
    private final Set<Integer> list;
    private Benchmark benchmark;
    Timer timer;

    public ThroughputStatistics(int numThreads, String filePath, int inter, int warmup, Set<Integer> list) {
        interval = inter;
        this.warmup = warmup;
        numT = numThreads;
        counters = new int[numThreads][interval + 1];
        countersPerOp = new int[3][numThreads][interval + 1];
        population = new int[interval+1];
        this.list = list;
        
        for (int i = 0; i < numThreads; i++) {
            for (int j = 0; j < interval + 1; j++) {
                counters[i][j] = 0;
                countersPerOp[ADD][i][j] = 0;
                countersPerOp[REMOVE][i][j] = 0;
                countersPerOp[CONTAINS][i][j] = 0;
            }
        }
        try {
            pw = new PrintWriter(new FileWriter(new File(filePath)));
        } catch (Exception e) {
            System.exit(0);
        }
    }

    public void computeThroughput(long timeMillis) {
        for (int time = 0; time <= interval; time++) {
            int total = 0;
            int totalAdd=0, totalRemove=0, totalContains=0;
            for (int i = 0; i < numT; i++) {
                total = total + counters[i][time];
                totalAdd = totalAdd + countersPerOp[ADD][i][time];
                totalRemove = totalRemove + countersPerOp[REMOVE][i][time];
                totalContains = totalContains + countersPerOp[CONTAINS][i][time];
            }
            
            float tp = (float) (total * 1000 / (float) timeMillis);
            float tpAdd = (float) (totalAdd * 1000 / (float) timeMillis);
            float tpRemove = (float) (totalRemove * 1000 / (float) timeMillis);
            float tpContains = (float) (totalContains * 1000 / (float) timeMillis);
            pw.println(time + "\t" +  tp  + "\t" +  tpAdd  + "\t" +  tpRemove  + "\t" +  tpContains  + "\t" +  population[time]);
        }
        pw.flush();
        System.out.println("Finished throughput measurements ...");
        try {
            System.out.println("Generating results");
            benchmark.generateResults();
            System.out.println("Stoping");
            benchmark.stop();
            timer.cancel();
        } catch (IOException ex) {
            Logger.getLogger(ThroughputStatistics.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printTP(long timeMillis) {
        int total = 0;
        for (int i = 0; i < numT; i++) {
            total = total + counters[i][now];
        }
        float tp = (float) (total * 1000 / (float) timeMillis);
        System.out.println("Throughput at " + tp + " ops/sec; pop at "+population[now]+" ops in sec : " + now);
    }

    boolean stoped = true;
    int fakenow = 0;
    public void start(Benchmark benchmark) {
        this.benchmark = benchmark;
        if (!started) {
            started = true;
            now = 0;
            System.out.println("Warming up ...");
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    fakenow++;
                    if(fakenow == warmup){
                        stoped = false;
                        for(int i = 0; i < numT; i++){
                            counters[i][0] = 0;
                        }
                        System.out.println("Started throughput measurements ...");
                    }else if (!stoped) {
                        
                        if (now <= interval) {
                            int size = 0;
                            size = list.size();
                            population[now] = size;
                            printTP(period);
                            now++;
                        }

                        if (now == interval + 1) {
                            stoped = true;
                            computeThroughput(period);
                        }
                    }
                }
            }, period, period);
            
        }
    }

    public void computeStatistics(int threadId, int amount, int op) {
        if(!stoped){
            counters[threadId][now] = counters[threadId][now] + amount;
            countersPerOp[op][threadId][now] = countersPerOp[op][threadId][now] + amount;
        }
    }

}
