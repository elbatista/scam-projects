package roleOfLocking;

import roleOfLocking.Lists.Set;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author elia.batista
 */
public class Benchmark {
    
    private Set<Integer> list;
    private int numThreads = 1, initListSize = 0, maxListSize = 1, interval = 60, warmup = 5;
    private Thread [] threads;
    private ThroughputStatistics statistics;
    private String dir;
    private boolean generateResults, generateGraphs;
    protected boolean stop = false;
    private final Results results = new Results();
    
    public Benchmark(int initListSize, int maxListSize, boolean generateGraphs) {
        this.initListSize = initListSize;
        this.maxListSize = maxListSize;
        this.generateGraphs = generateGraphs;
        createDirs();
    }

    public Benchmark(Set<Integer> list, int numThreads, int initListSize, int maxListSize, int interval, int warmup, boolean generateResults, boolean generateGraphs){
        this.list = list;
        this.numThreads = numThreads;
        this.threads = new Thread[numThreads];
        this.initListSize = initListSize;
        this.maxListSize = maxListSize;
        this.generateResults = generateResults;
        this.generateGraphs = generateGraphs;
        this.interval = interval;
        this.warmup = warmup;
        createDirs();
    }

    public void exec(boolean onlyResults){
        try {
            if(onlyResults){
                generateResults();
                return;
            }
            this.statistics = new ThroughputStatistics(numThreads, dir+"/"+this.list.getFileName(numThreads), this.interval, this.warmup, this.list);
            list.init(this.initListSize);
            statistics.start(this);
            startThreads();
            joinThreads();
        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void stop(){
        this.stop = true;
    }

    private void startThreads() {
        System.out.println("Starting "+this.numThreads+" threads");
        for(int i=0; i<this.numThreads; i++){
            this.threads[i] = new Thread(new WorkerThread(i));
        }
        for(Thread t : this.threads){
            t.start();
        }
    }

    private void joinThreads() throws InterruptedException {
        for(Thread t : this.threads){
            t.join();
        }
    }
    
    private void createDirs() {
        dir = "results";
        File dirFile = new File(dir);
        if (!dirFile.exists()) dirFile.mkdirs();
        dir += "/Ini" + (initListSize/1000) + "K_Max" +(maxListSize/1000)+"K";
        dirFile = new File(dir);
        if (!dirFile.exists()) dirFile.mkdirs();
    }
    
    public void generateResults() throws IOException {
        results.generateResults(dir, numThreads, generateResults, generateGraphs);
    }
    
    public void consolidateFiles() {
        try {
            results.consolidateFiles(dir, generateGraphs);
        } catch (IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private class WorkerThread implements Runnable {
        private final int id;
        public WorkerThread(int id){this.id = id;}
        private void execOperation(int item){
            int percentage = new Random().nextInt(100);
            if(percentage < 33){
                list.add(item);
                statistics.computeStatistics(this.id, 1, ThroughputStatistics.ADD);
            } else if(percentage < 66){
                list.remove(item);
                statistics.computeStatistics(this.id, 1, ThroughputStatistics.REMOVE);
            } else {
                list.contains(item);
                statistics.computeStatistics(this.id, 1, ThroughputStatistics.CONTAINS);
            }
        }
        @Override
        public void run() {
            while(!stop) execOperation(new Random().nextInt(maxListSize));
        }
    }
    
}