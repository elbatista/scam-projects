package roleOfLocking.Lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author elia.batista
 * @param <T>
 */
public class FineList <T> implements Set<T>{
    private class Node <T> {
        T item;
        int key;
        Node next;
        Lock lock;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
            lock = new ReentrantLock();
        }
        void lock(){lock.lock();}
        void unlock(){lock.unlock();}
    }
    private final Node head;
    private final AtomicInteger size = new AtomicInteger(0);
    public FineList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new Node(Integer.MAX_VALUE);
    }
    
    @Override
    public int size(){
        return size.get();
    }
    
    @Override
    public boolean add(T item) {
        int key = item.hashCode();
        head.lock();
        Node pred = head;
        try {
            Node curr = pred.next;
            curr.lock();
            try {
                while (curr.key < key) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    curr.lock();
                }
                if (curr.key == key) {
                    return false;
                }
                Node newNode = new Node(item);
                newNode.next = curr;
                pred.next = newNode;
                size.incrementAndGet();
                return true;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public boolean remove(T item) {
        Node pred = null, curr;
        int key = item.hashCode();
        head.lock();
        try {
            pred = head;
            curr = pred.next;
            curr.lock();
            try {
                while (curr.key < key) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    curr.lock();
                }
                if (curr.key == key) {
                    pred.next = curr.next;
                    size.decrementAndGet();
                    return true;
                }
                return false;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public boolean contains(T item) {
        Node pred = null, curr;
        int key = item.hashCode();
        head.lock();
        try {
            pred = head;
            curr = pred.next;
            curr.lock();
            try {
                while (curr.key < key) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    curr.lock();
                }
                if (curr.key == key) {
                    return true;
                }
                return false;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }
    
    @Override
    public String getFileName(int numThreads) {
        return "throughput_fine_"+numThreads+"t.txt";
    }
    
    @Override
    public void init(int iniSize){
        System.out.println("Initializing fine list with "+iniSize+" entries");
        Node pred, curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = node;
            pred = node;
            size.incrementAndGet();
        }
    }
}
