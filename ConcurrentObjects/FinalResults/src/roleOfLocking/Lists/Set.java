package roleOfLocking.Lists;

/**
 *
 * @author elia.batista
 */
public interface Set<T> {
    boolean add(T x);
    boolean remove(T x);
    boolean contains(T x);
    int size();
    String getFileName(int numThreads);
    void init(int iniSize);
}
