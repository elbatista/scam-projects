/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roleOfLocking.Lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author elbatista
 */
public class OptimisticList <T> implements Set<T>{

    private class Node <T> {
        T item;
        int key;
        Node next;
        Lock lock;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
            lock = new ReentrantLock();
        }
        void lock(){lock.lock();}
        void unlock(){lock.unlock();}
    }
    private final Node head;
    private final AtomicInteger size = new AtomicInteger(0);
    
    public OptimisticList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new Node(Integer.MAX_VALUE);
    }
    
    @Override
    public int size(){
        return size.get();
    }
    
    @Override
    public boolean add(T item) {
        int key = item.hashCode();
        while (true) {
            Node pred = head;
            Node curr = pred.next;
            while (curr.key <= key) {
                pred = curr; curr = curr.next;
            }
            pred.lock(); curr.lock();
            try {
                if (validate(pred, curr)) {
                    if (curr.key == key) {
                        return false;
                    } else {
                        Node node = new Node(item);
                        node.next = curr;
                        pred.next = node;
                        size.incrementAndGet();
                        return true;
                    }
                }
            } finally {
                pred.unlock(); curr.unlock();
            }
        }
    }

    @Override
    public boolean remove(T item) {
        int key = item.hashCode();
        while (true) {
            Node pred = head;
            Node curr = pred.next;
            while (curr.key < key) {
                pred = curr; curr = curr.next;
            }
            pred.lock(); curr.lock();
            try {
                if (validate(pred, curr)) {
                    if (curr.key == key) {
                        pred.next = curr.next;
                        size.decrementAndGet();
                        return true;
                    } else {
                        return false;
                    }
                }
            } finally {
                pred.unlock(); curr.unlock();
            }
        }
    }

    @Override
    public boolean contains(T item) {
        int key = item.hashCode();
        while (true) {
            Node pred = this.head; // sentinel node;
            Node curr = pred.next;
            while (curr.key < key) {
                pred = curr; curr = curr.next;
            }
            try {
                pred.lock(); curr.lock();
                if (validate(pred, curr)) {
                    return (curr.key == key);
                }
            } finally {
                // always unlock
                pred.unlock(); curr.unlock();
            }
        }
    }
    
    private boolean validate(Node pred, Node curr) {
        Node node = head;
        while (node.key <= pred.key) {
            if (node == pred)
                return pred.next == curr;
            node = node.next;
        }
        return false;
    }
    
    @Override
    public String getFileName(int numThreads) {
        return "throughput_opt_"+numThreads+"t.txt";
    }
    
    @Override
    public void init(int iniSize){
        System.out.println("Initializing optimistic list with "+iniSize+" entries");
        Node pred, curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = node;
            pred = node;
            size.incrementAndGet();
        }
    }
    
}
