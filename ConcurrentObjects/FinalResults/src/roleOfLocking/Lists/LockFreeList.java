package roleOfLocking.Lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 *
 * @author elbatista
 */
public class LockFreeList<T> implements Set<T>{
    
    private final AtomicInteger size = new AtomicInteger(0);
    private final Node head;
    
    public LockFreeList() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new AtomicMarkableReference(new Node(Integer.MAX_VALUE), false);
    }
    
    private class Node <T> {
        T item;
        int key;
        AtomicMarkableReference<Node> next;
        public Node(T item){
            this.item = item;
            this.key = item.hashCode();
            next = new AtomicMarkableReference(null, false);
        }
    }
    
    class Window {
        public Node pred, curr;
        Window(Node myPred, Node myCurr) {
            pred = myPred; curr = myCurr;
        }
    }
    
    public Window find(Node head, int key) {
        Node pred = null, curr = null, succ = null;
        boolean[] marked = {false};
        boolean snip;
        retry: while (true) {
            pred = head;
            curr = (Node) pred.next.getReference();
            while (true) {
                succ = (Node) curr.next.get(marked);
                while (marked[0]) {
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) continue retry;
                    curr = succ;
                    succ = (Node) curr.next.get(marked);
                }
                if (curr.key >= key)
                    return new Window(pred, curr);
                pred = curr;
                curr = succ;
            }
        }
    }
   
    @Override
    public int size(){
        return size.get();
    }

    @Override
    public boolean add(T item) {
        int key = item.hashCode();
        while (true) {
            Window window = find(head, key);
            Node pred = window.pred, curr = window.curr;
            if (curr.key == key) {
                return false;
            } else {
                Node node = new Node(item);
                node.next = new AtomicMarkableReference(curr, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    size.incrementAndGet();
                    return true;
                }
            }
        }
    }

    @Override
    public boolean remove(T item) {
        int key = item.hashCode();
        boolean snip;
        while (true) {
            Window window = find(head, key);
            Node pred = window.pred, curr = window.curr;
            if (curr.key != key) {
                return false;
            } else {
                Node succ = (Node) curr.next.getReference();
                snip = curr.next.compareAndSet(succ, succ, false, true);
                if (!snip)
                    continue;
                pred.next.compareAndSet(curr, succ, false, false);
                size.decrementAndGet();
                return true;
            }
        }
    }

    @Override
    public boolean contains(T item) {
        boolean[] marked = {false};
        int key = item.hashCode();
        Node curr = head;
        while (curr.key < key) {
            curr = (Node) curr.next.getReference();
            Node succ = (Node) curr.next.get(marked);
        }
        return (curr.key == key && !marked[0]);
    }
    
    @Override
    public String getFileName(int numThreads) {
        return "throughput_lockFree_"+numThreads+"t.txt";
    }
    
    @Override
    public void init(int iniSize){
        System.out.println("Initializing lock free list with "+iniSize+" entries");
        Node pred;
        AtomicMarkableReference curr;
        pred = head;
        curr = head.next;
        for(Integer value = 1; value <= iniSize; value++){
            Node node = new Node(value);
            node.next = curr;
            pred.next = new AtomicMarkableReference(node, false);
            pred = node;
            size.incrementAndGet();
        }
    }
}
