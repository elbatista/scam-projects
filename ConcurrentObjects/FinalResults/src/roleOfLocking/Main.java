package roleOfLocking;

import roleOfLocking.Lists.CoarseList;
import roleOfLocking.Lists.OptimisticList;
import roleOfLocking.Lists.LazyList;
import roleOfLocking.Lists.LockFreeList;
import roleOfLocking.Lists.FineList;
import roleOfLocking.Lists.SequentialList;

/**
 * @author elia.batista
 * 
 *  Initial Benchmark Params:
 * - Initial list size: 100K
 * - Max list size: 200K
 * - Workload: add, remove, contains (1/3 each, balanced))
 * - N Threads: variable
 * 
 * Metrics:
 * - Throughput
 * - List size (population)
 * - Throughput per operation
 * 
 * Ps.: implement getSize() using an atomic variable
 */
public class Main {
    public static void main(String args[]){
        
        if (args.length < 4) {
            System.out.println(
            "Usage: ./run.sh <ini list size> <max list sizes: [s1-s2-s3]> <warmup (sec)> <measurement interval (sec)> <onlyResults?>\n"+
            "Example: ./run.sh 100000 200000-300000 5 30 false");
            System.exit(-1);
        }
        
        int iniListSize = Integer.parseInt(args[0]);
        String maxListSize[] = args[1].split("-");
        int numThreads[] = {2,4,8,16,32,64,128};
        int warmupTime = Integer.parseInt(args[2]);
        int intervalExecution = Integer.parseInt(args[3]); 
        boolean onlyResults = Boolean.valueOf(args[4]);
        
        for(String listSize : maxListSize){
            new Benchmark(new SequentialList<>(), 1, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, false, false).exec(onlyResults);
            for(int threads : numThreads){
                new Benchmark(new CoarseList<>(), threads, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, false, false).exec(onlyResults);
                new Benchmark(new FineList<>(), threads, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, false, false).exec(onlyResults);
                new Benchmark(new OptimisticList<>(), threads, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, false, false).exec(onlyResults);
                new Benchmark(new LazyList<>(), threads, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, false, false).exec(onlyResults);
                new Benchmark(new LockFreeList<>(), threads, iniListSize, Integer.valueOf(listSize), intervalExecution, warmupTime, true, true).exec(onlyResults);
            }
            new Benchmark(iniListSize, Integer.valueOf(listSize), true).consolidateFiles();
        }
    }
}
